package com.crea.leonardo.heroapp.utils;

import android.util.Log;

public class Util {

    public static void logTag(String tag, String message){
        Log.d(tag, message);
    }
}
