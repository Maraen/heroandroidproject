package com.crea.leonardo.heroapp.interfaces;

import com.crea.leonardo.heroapp.model.Hero;

import java.util.ArrayList;

public interface HeroServiceListener {

    public void responseWithSuccess(ArrayList<Hero> heros);
    public void responseWithError(String errorMesssage );
}
